from django.conf.urls import url

from contacts import views

urlpatterns = [
    url(r'^addcontact/', views.addcontact),
    url(r'^thanks/', views.thanks),
    url(r'^', views.contact),
]