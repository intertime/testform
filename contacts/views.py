# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponseRedirect, render_to_response
from django.http.response import HttpResponse, Http404
from django.template.context_processors import csrf
from django.template import Context
from models import Contacts
from forms import ContactForm
from django import forms
from TestForm.views import lf
from django.contrib import auth


def contact(request):
    form = ContactForm()
    args = {}
    args.update((csrf(request)))
    args['form'] = form
    args['lf'] = lf()
    args['username'] = auth.get_user(request).username
    return render_to_response('contacts.html', args)


def thanks(request):
    return render_to_response('thanks.html', {'lf': lf(), 'username': auth.get_user(request).username})


def addcontact(request):
    if request.POST:
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/contacts/thanks/')
    else:
        form = ContactForm()
    return render(request, 'contacts.html', {'form': form, 'lf': lf(), 'username': auth.get_user(request).username})
