# Create your models here.
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import datetime


# Create your models here.

class Contacts(models.Model):
    class Meta:
        db_table = "contacts"

    contacts_date = models.DateTimeField(default=datetime.now())
    contacts_name = models.CharField(max_length=50)
    contacts_email = models.EmailField()
    contacts_message = models.TextField(max_length=1000)
