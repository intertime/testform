# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django import forms
from models import Contacts

'''
class SomeForm(forms.ModelForm):
    class Meta:

        # модель, по которой будет строиться форма
        model = SomeModel

        # поля, которые должны быть в форме
        fields = ()

        # поля, которых не должно быть в форме
        exclude = ()

        # словарь, надписи для элементов
        labels = {
            'field_name': 'label',
        }

        # словарь, вспомогательный текст для элементов
        help_texts = {
            'field_name': 'help_text',
        }

        # словарь, виджеты для элементов
        widgets = {
            'field_name': forms.HiddenInput,
        }

        # словарь сообщений для кодов ошибок
        # required, min_length, max_length, invalid_choice, invalid,
        # min_value, max_value
        error_messages = {
            'field_name': {
                'error_code': 'text',
            },
        }
'''


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contacts
        fields = ('contacts_name', 'contacts_email', 'contacts_message')
        labels = {
            'contacts_name': u'Ваше имя', 'contacts_email': u'Ваш email', 'contacts_message': u'Ваше сообщение',
        }
        help_texts = {
            'contacts_name': u'не более 10 символов',
        }