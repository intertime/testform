from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^logforms/$', views.logforms),
    #url(r'^(?P<form_name>\w+)/$', views.form1),
    url(r'^form1/create_form/$', views.create_form),
    url(r'^form1/edit_form/(?P<form_id>\d+)/$', views.edit_form),
    url(r'^form1/delete_form/(?P<form_id>\d+)/$', views.delete_form),
]
