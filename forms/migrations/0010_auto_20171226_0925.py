# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-26 06:25
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0009_auto_20171222_2248'),
    ]

    operations = [
        migrations.CreateModel(
            name='ListForms',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_en', models.CharField(max_length=100)),
                ('name_ru', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('type', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'listforms',
            },
        ),
        migrations.AlterField(
            model_name='form1',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 26, 9, 25, 12, 228000)),
        ),
        migrations.AlterField(
            model_name='form2',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 26, 9, 25, 12, 228000)),
        ),
        migrations.AlterField(
            model_name='form3',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 26, 9, 25, 12, 229000)),
        ),
    ]
