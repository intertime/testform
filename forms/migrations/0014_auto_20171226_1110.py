# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-26 08:10
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0013_auto_20171226_1107'),
    ]

    operations = [
        migrations.RenameField(
            model_name='listforms',
            old_name='name_en',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='listforms',
            old_name='name_ru',
            new_name='table_name',
        ),
        migrations.AlterField(
            model_name='form1',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 26, 11, 10, 14, 706000)),
        ),
        migrations.AlterField(
            model_name='form2',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 26, 11, 10, 14, 707000)),
        ),
        migrations.AlterField(
            model_name='form3',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2017, 12, 26, 11, 10, 14, 708000)),
        ),
    ]
