# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import datetime


# Create your models here
class ListForms(models.Model):
    class Meta:
        db_table = "listforms"

    table_name = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    description = models.TextField()
    business = models.BooleanField(default=False)

    def __str__(self):
        return self.table_name


class Form1(models.Model):
    class Meta:
        db_table = "form1"

    number = models.IntegerField(default=0)
    date = models.DateTimeField(default=datetime.now())
    title = models.CharField(max_length=200)
    text = models.TextField()
    type = models.IntegerField(default=0)
    url = models.CharField(max_length=100, default='/')



class Form2(models.Model):
    class Meta:
        db_table = "form2"

    number = models.IntegerField(default=0)
    date = models.DateTimeField(default=datetime.now())
    title = models.CharField(max_length=200)
    text = models.TextField()
    text2 = models.TextField()
    type = models.IntegerField(default=1)
    url = models.CharField(max_length=100, default='/')


class Form3(models.Model):
    class Meta:
        db_table = "form3"

    number = models.IntegerField(default=0)
    number2 = models.IntegerField(default=777)
    date = models.DateTimeField(default=datetime.now())
    title = models.CharField(max_length=200)
    text = models.TextField()
    type = models.IntegerField(default=2)
    url = models.CharField(max_length=100, default='/')
