# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Form1, ListForms


class Form1Admin(admin.ModelAdmin):
    fields = ['form1_number', 'form1_title']


class ListFormsAdmin(admin.ModelAdmin):
    fields = ['table_name', 'name', 'description', 'business']


admin.site.register(Form1, Form1Admin)
admin.site.register(ListForms, ListFormsAdmin)
