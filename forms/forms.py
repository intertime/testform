# -*- coding: utf-8 -*-
from django.forms import ModelForm
from models import Form1


class CreateForm1(ModelForm):
    class Meta:
        model = Form1
        fields = {'number', 'date', 'title', 'text'}
        labels = {'number': u'№ документа', 'date': u'Дата', 'title': u'Наименование',
                  'text': u'Содержание'
                  }
