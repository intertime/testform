# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponseRedirect, render_to_response, redirect, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import HttpResponse, Http404
from django.template.context_processors import csrf
from django.template.loader import get_template
from django.template import Context
from django.contrib import auth
from django.core.paginator import Paginator
from models import Form1, Form2, Form3, ListForms
from forms import CreateForm1


def lf(): return ListForms.objects.all()


def logforms(request):
    logform = Form1.objects.all()

    '''if form_name == 'form1':
        all_forms = Form1.objects.all()
    elif form_name == 'form2':
        all_forms = Form2.objects.all()
    elif form_name == 'form3':
        all_forms = Form3.objects.all()
        '''

    return render_to_response('logforms.html', {'logform': logform, 'lf': lf(), 'username': auth.get_user(request).username})


def create_form(request):
    var = 'create_form'
    if request.POST:
        form = CreateForm1(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = CreateForm1()
    return render(request, 'form1.html', {'form': form, 'var': var, 'lf': lf(), 'username': auth.get_user(request).username})


def edit_form(request, form_id):
    var = 'edit_form' + '/' + form_id
    post = get_object_or_404(Form1, id=form_id)
    if request.POST:
        form = CreateForm1(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = CreateForm1(instance=post)
    return render(request, 'form1.html', {'form': form, 'var': var, 'lf': lf(), 'username': auth.get_user(request).username})


def delete_form(request, form_id):
    try:
        f = Form1.objects.get(id=form_id)
        f.delete()
        response = redirect('/')
        return response
    except ObjectDoesNotExist:
        raise Http404
    return redirect('/')
