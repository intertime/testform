# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render_to_response
from django.template import Context
from django.contrib import auth
from forms.views import lf
from django.contrib.auth import authenticate


def index(request):
    return render_to_response('index.html', {'lf': lf(), 'username': auth.get_user(request).username})


def about(request):
    return render_to_response('about.html', {'lf': lf(), 'username': auth.get_user(request).username})


def help(request):
    return render_to_response('help.html', {'lf': lf(), 'username': auth.get_user(request).username})
